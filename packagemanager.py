#!/usr/bin/python3
import os
class config:
	def __init__(self):
		self.destdir="/data/user/sulin/test/destdir"
		self.infodir=self.destdir+"/var/lib/pkgs/info"
		self.repourl=""
		self.archivedir=self.destdir+"/var/lib/pkgs/archive"
		self.filetype="zip"
		self.bufferdir=self.destdir+"/var/buffer/"
		for dirs in [self.destdir,self.infodir,self.archivedir,self.bufferdir]:
			os.system("mkdir -p {} 2>/dev/null".format(dirs))

class database:
	def __init__(self):
		self.list_installed=[]
		self.need_install=[]
	def refresh(self):
		self.list_installed=os.listdir(config.infodir)

config=config()
db=database()		

class package:
	def __init__(self):
		self.name=""
		self.version=0
		self.type=""
		self.size=0
		self.url=""
		self.dep=[]
		self.installed=False
		self.pkg_url=""
		self.arch="none"
		self.archive_location=""
		
	def get_val(self,line):
		return line.split("::")[1].replace("\n","")
		
	def load_from_repo(self,url):
		if (0==fetch().fetch(url,"/tmp/info")):
			self.load_from_file("/tmp/info")
			os.unlink("/tmp/info")
		else:
			print("Fetch error")
			exit(1)
		
	def load_from_db(self,name):
		self.load_from_file("{0}/{1}/info".format(config.infodir,name))
	
	def load_from_file(self,metadata):
		f=open(metadata,"r").readlines()
		self.load(f)
		
	def load_from_archive(self,archive):
		ret=os.system("""
		unzip -p {} info > /tmp/info
		""".format(archive))
		if(ret==0):
			self.load_from_file("/tmp/info")
			os.unlink("/tmp/info")
		
	def load(self,f):
		for line in f:
			if line[0] == "#" or line[0] == " " or line[0] == "\t":
				pass
			elif "name:" in line:
				self.name=self.get_val(line)
			elif "version:" in line:
				self.version=int(self.get_val(line))
			elif "url:" in line:
				self.url=self.get_val(line)
			elif "arch:" in line:
				self.arch=self.get_val(line)
			elif "type:" in line:
				self.type=self.get_val(line)
			elif "size:" in line:
				self.size=int(self.get_val(line))
			elif "dep:" in line:
				self.dep=self.get_val(line).split(" ")
		if(os.path.isfile("{}/{}.info".format(config.infodir,self.name))):
			self.installed=True
		self.pkg_url="{}/{}".format(config.repourl,self.name)
		self.archive_location="{}/{}-{}-{}.{}".format(config.archivedir,self.name,self.version,self.arch,config.filetype)
		
		
	def unpack(self,location,subdir="data"):
		os.system("""
		mkdir -p {0} 2>/dev/null
		cd {0}
		rm -f data.tar.xz 2>/dev/null
		unzip -o {1} &>/dev/null
		rm -rf [2] 2> /dev/null
		mkdir {2} 2>/dev/null
		cd {2}
		tar -xf ../data.tar.xz
		""".format(config.bufferdir,location,subdir))
	def copy_files(self,subdir="data"):
		os.system("""
		cd {0}/{1}
		cp -pifr * {2}
		cd ..
		mkdir -p {3}/{1} 2>/dev/null
		install info {3}/{1}/info
		install paths {3}/{1}/paths
		install ops {3}/{1}/ops
		""".format(config.bufferdir,subdir,config.destdir,config.infodir))
	def remove_files(self,subdir="data"):
		os.system("""
		cat {0}/{1}/paths | while read line
		do
			[ -f {2}/$line ] && rm -f {2}/$line
		done
		cat {0}/{1}/paths | while read line
		do
			[ -d {2}/$line ] && rmdir {2}/$line
		done
		rm -rf {0}/{1}
		""".format(config.infodir,subdir,config.destdir))
	def pack(self,path):
		p=package()
		p.load_from_file("{}/info".format(path))
		os.system("""
		cd {0}/files
		find | sed "s/^.//g" | grep "^/" > ../paths
		tar --xz -cf data.tar.xz *
		cd ..
		mv files/data.tar.xz data.tar.xz
		zip -u {1}-{2}-{3}.zip data.tar.xz info paths ops &>/dev/null
		rm -f data.tar.xz
		""".format(path,p.name,p.version,p.arch))
		
class fetch:
	def fetch(self,url,name):
		ret=0
		if os.path.exists(name):
			return ret
		if "git://" in url:
			ret=os.system("git clone --depth=1 \"{}\" \"{}\"".format(url,name))
		else:
			ret=os.system("curl \"{}\" > \"{}\"".format(url,name))
		return ret


class resolve_depends:
	def check_install(self,source):
		if(source.installed==True or source in db.need_install):
			return True
		else:
			return False
	
	def get_deps(self,source):
		if(not self.check_install(source)):
			db.need_install.append(source)
			for pkgname in source.dep:
				pkg=package()
				pkg.load_from_file(pkgname+".info")
				if(not self.check_install(pkg)):
					db.need_install.append(pkg)
					self.get_deps(pkg)


class operation:
		def install_archive(self,path):
			os.system("""
			unzip {0} 
			""")
		
		def install(self,name):
			p=package()
			p.load_from_archive(name)
			os.system("cp -prf {0} {1}/$(basename {0})".format(name,config.archivedir))
			db.need_install=[]
			resolve_depends().get_deps(p)
			for pkg in db.need_install:
				self.download_single(pkg)
			for pkg in db.need_install:
				self.unpack_single(pkg)
			for pkg in db.need_install:
				self.copy_single(pkg)
			os.system("rm -rf {}".format(config.bufferdir))

		def download_single(self,pkg):
			fetch().fetch(pkg.pkg_url,pkg.archive_location)
		
		def unpack_single(self,pkg):
			pkg.unpack(pkg.archive_location,pkg.name)

		def copy_single(self,pkg):
			pkg.copy_files(pkg.name)
			
		def remove_single(self,name):
			pkg=package()
			pkg.load_from_db(name)
			pkg.remove_files(pkg.name)

		def pack(self,path):
			package().pack(path)


operation().pack("hello")
operation().install("hello/hello-1-none.zip")
